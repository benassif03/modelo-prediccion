# Modelo de Predicción del Potencial de Jugadores de Fifa 2022
# Franco Benassi
from sklearn.model_selection import train_test_split as tts
from sklearn.linear_model import LinearRegression as lr
from sklearn.metrics import mean_squared_error as mse
import matplotlib.pyplot as pt
import pandas as pd
import numpy as np

df = pd.read_csv('fifa22.csv')
filas = df.head(4)      # Muestra las primeras 4 filas del csv
#print(filas)

distri = df.describe() # Muestra como están distribuidos los datos
#print("La distribución es:", distri)

# Evaluar si hay valores nulos en las columnas de edad y potencial
print("Existen valores nulos?:", df['age'].isnull().any(), df['potential'].isnull().any())

# Grafica del potencial de jugadores con respecto a su edad
pt.scatter(data=df, x='age', y='potential')
pt.xlabel("Edad")
pt.ylabel("Potencial")
pt.show()

# Mapa de calor que muestra la mayor edad donde se concentran los jugadores
pt.hist2d(data=df, x='age', y='potential')
pt.xlabel("Edad")
pt.ylabel("Potencial")
pt.colorbar()
pt.show()

# Datos de Entrenamiento: xEntrena = edad, yEntrena = potencial
# Datos de Testing (Prueba): xPrueba = edad, yPrueba = potencial
# Los datos de edades y potenciales se dividen en dos bloques, uno de entrenamiento y el otro de prueba
# El primer resultado mostrará la cantidad de jugadores para entrenar y el siguiente la de jugadores para poner a prueba 
xEntrena, xPrueba, yEntrena, yPrueba = tts(df['age'], df['potential'], test_size=0.2)
print(xEntrena, xPrueba, yEntrena, yPrueba)
print(xEntrena.shape, yEntrena.shape)
#print(type(xEntrena))

xEntrena = np.array(xEntrena)   # Convertir en ndarray para que trabaje bien con sklearn
yEntrena = np.array(yEntrena)
xPrueba = np.array(xPrueba)
xEntrena = xEntrena.reshape(-1, 1) # Conversión a arreglos 2D para yEntrena y xEntrena
yEntrena = yEntrena.reshape(-1, 1)
xPrueba = xPrueba.reshape(-1, 1)
print(xEntrena.shape)

# Modelo de regresión lineal
regresion = lr()

# Entrenamiento
entrenamiento = regresion.fit(xEntrena, yEntrena)
print(entrenamiento)

# Predicción
yPrediccion = regresion.predict(xPrueba)

print("Las edades de los jugadores son:", xPrueba) # Edades de los jugadores
print('\n')
print("Los potenciales predichos de los jugadores son:", yPrediccion) # Potencial de los jugadores según predicción

# Verificar si la predicción es correcta, yPrueba: potenciales reales
verificacion = mse(yPrueba, yPrediccion)
print("El margen de error es de:", verificacion)
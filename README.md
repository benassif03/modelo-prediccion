# Modelo de Predicción del Potencial de Jugadores de Fifa 22

## Autor
**Franco Benassi**

## Python
Este modelo fue desarrollado en el lenguaje de programación Python.

## Librerías
El modelo utiliza las siguientes librerías:
- scikit-learn
- numpy
- pandas
- matplotlib

## Modelo
El modelo obtiene sus datos de un archivo csv obtenido de la comunidad online [Kaggle](https://www.kaggle.com/datasets/stefanoleone992/fifa-22-complete-player-dataset).

Comienza evaluando si existen valores nulos en las carecterísticas elegidas de los jugadores las cuales corresponden a la edad y el potencial.

Luego, se grafica mediante matplotlib el potencial de los jugadores respecto a su edad obteniendo como resultado:

![Gráfica](Grafica.png)

Según el gráfico el potencial más alto se ubica con los jugadores más jóvenes que rondan desde los 17 a 25 años.

Se hizo un mapa de calor que indique de una forma más precisa cual es la edad donde existe una mayor cantidad de jugadores, el resultado fue:

![Mapa](mapaCalor.png)

El mapa indicó que la gran mayoría de los jugadores ronda los 20 años.

## Predicción
Este proceso se basa en que cuenta con datos de entrenamiento y datos de prueba.

Los datos de edades y potenciales se dividen en dos bloques, uno de entrenamiento y el otro de prueba.

El primer resultado mostrará la cantidad de jugadores para entrenar y el siguiente la de jugadores para poner a prueba.

Luego, los datos se convierten en arreglos de dos dimensiones debido a que tanto el entrenamiento como la predicción lo requieren.

Para predecir los potenciales se utilizarán las edades de los jugadores mediante regresión lineal.

Por último el modelo verifica si el resultado obtenido por la predicción es correcto.